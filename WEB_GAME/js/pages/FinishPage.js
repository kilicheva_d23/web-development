class FinishPage {
    initializeObjects() {
        // Create overlay for finish page
        this.finishPageOverlay = document.createElement("div");
        this.finishPageOverlay.className = "finish_page_overlay";

        // Create finish page card
        this.finishPageCard = document.createElement("div");
        this.finishPageCard.className = "finish_page_card";

        // Create image of logo
        this.logo = document.createElement("img");
        this.logo.setAttribute("src", "./img/logo.png");
        this.logo.setAttribute("alt", "Logo");
        this.logo.className = "logo";

        // Create label "Top Scores"
        this.label = document.createElement("span");
        this.label.innerText = "Top Scores";
        this.label.className = "label";

        // Get all players' name and scores
        const players = JSON.parse(localStorage.getItem("players")) == null ? [] : JSON.parse(localStorage.getItem("players"));

        // Create top scores table
        this.scoreTable = document.createElement("table");
        this.scoreTable.className = "score_table";

        let rows = `
            <tr>
                <th></th>
                <th>Player Name</th>
                <th>Score</th>
            </tr>
        `;

        players.forEach((el, index) => {
            rows += `
                <tr>
                    <td>${(index + 1)}</td>
                    <td>${el.name}</td>
                    <td>${el.score} ${index == 0 ? "<span class=\"stars\">***</span>" : index == 1 ? "<span class=\"stars\">**</span>" : index == 2 ? "<span class=\"stars\">*</span>" : " " }</td>
                </tr>
            `;
        });

        for (let a = players.length + 1; a <= 10; a++) {
            rows += `
                <tr>
                    <td>${a}</td>
                    <td> </td>
                    <td> </td>
                </tr>
            `;
        }


        this.scoreTable.innerHTML = rows;

        // Create new game button
        this.newGameBtn = document.createElement("button");
        this.newGameBtn.className = "new_game_button";
        this.newGameBtn.innerText = "New Game";

        // Append all objects to start page card
        this.finishPageCard.appendChild(this.logo);
        this.finishPageCard.appendChild(this.label);
        this.finishPageCard.appendChild(this.scoreTable);
        this.finishPageCard.appendChild(this.newGameBtn);

        // Append start page card to overlay
        this.finishPageOverlay.appendChild(this.finishPageCard);
    }

    showPage(timer, gamePage, finishPage) {
        this.initializeObjects();
        this.setListeners(timer, gamePage, finishPage);
        document.querySelector(".game_container").appendChild(this.finishPageOverlay);
    }

    hidePage() {
        this.finishPageOverlay.remove();
    }

    setListeners(timer, gamePage, finishPage) {
        // Listen for click of new button and remove finish page
        this.newGameBtn.addEventListener("click", (e) => {
            this.hidePage();
            location.reload();
        });
    }
}