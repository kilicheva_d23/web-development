class StartPage {
    initializeObjects(timer, gamePage, finishPage) {
        // Create overlay for start page
        this.startPageOverlay = document.createElement("div");
        this.startPageOverlay.className = "start_page_overlay";

        // Create start page card
        this.startPageCard = document.createElement("div");
        this.startPageCard.className = "start_page_card";

        // Create image of logo
        this.logo = document.createElement("img");
        this.logo.setAttribute("src", "./img/logo.png");
        this.logo.setAttribute("alt", "Logo");
        this.logo.className = "logo";

        // Create label "Enter your name"
        this.label = document.createElement("span");
        this.label.innerText = "Enter your name";
        this.label.className = "label";

        // Create player name input
        this.playerName = document.createElement("input");
        this.playerName.setAttribute("type", "text");
        this.playerName.setAttribute("autofocus", "true");
        this.playerName.addEventListener("keypress", e => {
            if (e.code == "Enter" || e.code == "NumpadEnter") {
                const name = this.playerName.value.trim();

                // Check if the input is not empty
                if (name !== "") {
                    // Set current player value in Local Storage
                    localStorage.setItem("currentPlayer", name);

                    this.hidePage();
                    timer.startTimer(gamePage, finishPage);
                } else {
                    this.playerName.value = "";
                }
            }
        });
        this.playerName.className = "player_name";

        // Create start button
        this.startBtn = document.createElement("button");
        this.startBtn.className = "start_button";
        this.startBtn.innerText = "Start";

        // Append all objects to start page card
        this.startPageCard.appendChild(this.logo);
        this.startPageCard.appendChild(this.label);
        this.startPageCard.appendChild(this.playerName);
        this.startPageCard.appendChild(this.startBtn);

        // Append start page card to overlay
        this.startPageOverlay.appendChild(this.startPageCard);
    }

    showPage(timer, gamePage, finishPage) {
        this.timer = timer;
        this.initializeObjects(timer, gamePage, finishPage);
        this.setListeners(gamePage, finishPage);
        document.querySelector(".game_container").appendChild(this.startPageOverlay);
    }

    hidePage() {
        this.startPageOverlay.remove();
    }

    setListeners(gamePage, finishPage) {
        // Listen for click of start button and check the input
        this.startBtn.addEventListener("click", (e) => {
            const name = this.playerName.value.trim();

            // Check if the input is not empty
            if (name !== "") {
                // Set current player value in Local Storage
                localStorage.setItem("currentPlayer", name);

                this.hidePage();
                this.timer.startTimer(gamePage, finishPage);
            } else {
                this.playerName.value = "";
            }
        });

        // Control case and size of player name
        this.playerName.addEventListener("input", (e) => {
            // Change player name case to upper
            e.target.value = e.target.value.toUpperCase();

            // Limit player name size
            const wordLimit = 20;

            if (e.target.value.length > wordLimit) {
                e.target.value = e.target.value.substring(0, wordLimit);
            }
        });
    }
}