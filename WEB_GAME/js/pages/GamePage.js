class GamePage {
    initializeObjects(fishFactory) {
        this.fishFactory = fishFactory;
        this.obj = [];
        // Create info panel section
        this.infoPanel = document.createElement("div");
        this.infoPanel.className = "info_panel";

        // Create span showing player name
        this.nameLabel = document.createElement("span");
        this.nameLabel.className = "name_label";
        this.nameLabel.innerText = "Player Name: " + localStorage.getItem("currentPlayer");

        // Create span showing score
        this.scoreLabel = document.createElement("span");
        this.scoreLabel.className = "score_label";
        this.scoreLabel.innerHTML = "Score:";

        const scoreNum = document.createElement("span");
        scoreNum.className = "score";
        scoreNum.id = "score";
        scoreNum.innerText = "0";

        this.scoreLabel.appendChild(scoreNum);

        // Create span showing timer
        this.timeLabel = document.createElement("span");
        this.timeLabel.className = "time_label";
        this.timeLabel.id = "timeLabel";
        this.timeLabel.innerText = "00:30";

        this.infoPanel.appendChild(this.nameLabel);
        this.infoPanel.appendChild(this.scoreLabel);
        this.infoPanel.appendChild(this.timeLabel);

        const gameContainer = document.querySelector(".game_container");

        for (let i = 0; i < 10; i++) {
            const fish = fishFactory.createFish();
            gameContainer.appendChild(fish);
            this.obj.push(fish);
        }

        const fishListener = (e) => {
            if (e.target.className == "fish") {
                let score = parseInt(scoreNum.innerText);
                score = score + parseInt(e.target.score);
                scoreNum.innerText = score;

                this.obj = this.obj.filter(fish => {
                    if (fish == e.target) {
                        return false;
                    }
                    return true;
                });
                e.target.remove();

                const fish = fishFactory.createFish();

                this.obj.push(fish);
                gameContainer.appendChild(fish);
            }
        };

        gameContainer.addEventListener("click", fishListener);

        this.listenFishClick = fishListener;

        this.movingFish = setInterval(() => {
            this.obj.forEach(fish => {
                fishFactory.move(fish);
                if (parseInt(fish.style.left.replace("px", "")) > innerWidth || parseInt(fish.style.left.replace("px", "")) < 0) {
                    this.obj = this.obj.filter(e => e != fish);
                    fish.remove();
                    const newFish = fishFactory.createFish();
                    this.obj.push(newFish);

                    gameContainer.appendChild(newFish);
                }
            });
        }, 100);
    }

    showPage(fishFactory) {
        this.initializeObjects(fishFactory);
        document.querySelector(".game_container").appendChild(this.infoPanel);
    }

    hidePage() {
        const name = localStorage.getItem("currentPlayer");
        const score = document.querySelector(".score").innerText;

        let players = localStorage.getItem("players") ? JSON.parse(localStorage.getItem("players")) : [];

        players.push({ name, score });
        players.sort((a, b) => (b.score - a.score));

        if (players.length > 10)
            players.pop();

        localStorage.setItem("players", JSON.stringify(players));
        clearInterval(this.movingFish);
        const fishArr = document.querySelectorAll(".fish");
        fishArr.forEach(e => e.remove());
        this.obj = [];
        const gameContainer = document.querySelector(".game_container");
        gameContainer.removeEventListener("click", this.listenFishClick);

        this.infoPanel.remove();
    }

}