class Fish {
    createFish() {
        const params = this.getFishParams();
        let fish = document.createElement("img");
        fish.style.position = "absolute";
        fish.className = "fish";
        fish.score = params.score;
        fish.width = params.w;
        fish.height = params.h;
        fish.src = params.url;
        fish.setAttribute("dir", params.dir);
        fish.speed = params.speed;
        fish.style.top = params.top;
        fish.style.left = params.left;
        fish.style.cursor = "pointer";

        setTimeout(function() {
            fish.speed = 50;
        }, 10000);

        let paramLeft = parseInt(params.left.replace("px", ""));

        if ((paramLeft > innerWidth / 2 && params.dir == "right") || (paramLeft < innerWidth / 2 && params.dir == "left")) {
            this.changeDirection(fish);
        }
        return fish;
    }

    getFishParams() {
        const fishParams = [
            { score: 35, w: 65, h: 35, url: "./img/small_fish.png", dir: "left", speed: this.randomSpeed(), top: this.randomTop(), left: this.randomLeft() },
            { score: 30, w: 130, h: 90, url: "./img/medium_fish.png", dir: "right", speed: this.randomSpeed(), top: this.randomTop(), left: this.randomLeft() },
            { score: 25, w: 120, h: 120, url: "./img/large_fish.png", dir: "right", speed: this.randomSpeed(), top: this.randomTop(), left: this.randomLeft() },
            { score: 20, w: 270, h: 320, url: "./img/xlarge_fish.png", dir: "left", speed: this.randomSpeed(), top: this.randomTop(), left: this.randomLeft() },
            { score: 15, w: 300, h: 200, url: "./img/xxlarge_fish.png", dir: "left", speed: this.randomSpeed(), top: this.randomTop(), left: this.randomLeft() }
        ];

        let randomIndex = Math.floor(Math.random() * Math.floor(5));
        return fishParams[randomIndex];
    }

    move(fish) {
        let fishLeft = parseInt(fish.style.left.replace("px", ""));
        if (fish.getAttribute("dir") == "right") {
            fishLeft += fish.speed;
        } else {
            fishLeft -= fish.speed;
        }

        fish.style.left = fishLeft + "px";
    }

    changeDirection(fish) {
        fish.style.transform = "scaleX(-1)";
        if (fish.getAttribute("dir") === "right")
            fish.setAttribute("dir", "left");
        else
            fish.setAttribute("dir", "right");
    }

    randomSpeed() {
        return Math.floor(Math.random() * (20 - 5) + 5);
    }

    randomTop() {
        return Math.floor(Math.random() * (innerHeight - 320) + 60) + "px";
    }

    randomLeft() {
        return Math.floor(Math.random() * (innerWidth - 300) + 15) + "px";
    }
}