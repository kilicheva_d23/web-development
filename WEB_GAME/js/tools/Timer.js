class Timer {
    startTimer = (gamePage, finishPage) => {
        gamePage.showPage(new Fish());
        this.time = 30;
        this.changeTime();
        const interval = setInterval(this.changeTime, 1000);

        const timer = this;
        setTimeout(() => {
            clearInterval(interval);
            gamePage.hidePage();
            finishPage.showPage(timer, gamePage, finishPage);
        }, (this.time + 1) * 1000);
    }

    changeTime = () => {
        document.getElementById("timeLabel").innerText = `00:${this.time > 9 ? this.time : "0" + this.time}`;
        this.time--;
    }
}